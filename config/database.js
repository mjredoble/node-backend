//Set up mongoose connection
const mongoose = require('mongoose');
const mongoDB = 'mongodb+srv://dbUser:1234567890@cluster0.3c7gk.mongodb.net/MyDB?retryWrites=true&w=majority';
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.Promise = global.Promise;

module.exports = mongoose;